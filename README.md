ETL Web Crawler

This is an example of the ETL (Extract Transform Load) technique usage, 
a web crawler (spider) that grabs web pages and stores them locally, 
using a feed list populated within the crawl process.
The pages are then processed in the transformation phase, resulting an index
database in the JSON format, which someone can use to make a boolean or a
vectorial search.