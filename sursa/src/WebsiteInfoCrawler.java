import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class WebsiteInfoCrawler {
    private String htmlFile;
    private String baseUri;
    private Document doc;

    WebsiteInfoCrawler(String htmlFile, String baseUri)
    {
        this.htmlFile = htmlFile;
        this.baseUri = baseUri;
        try {
            doc = Jsoup.parse(new File(htmlFile), null, baseUri);
        } catch (IOException e)
        {
            doc = null;
        }
    }

    public Document getDoc()
    {
        return doc;
    }

    public String getBaseUri()
    {
        return baseUri;
    }

    public String getText()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getTitle()); // titlul
        sb.append(System.lineSeparator());
        sb.append(getKeywords()); // cuvintele cheie
        sb.append(System.lineSeparator());
        sb.append(getDescription());
        sb.append(System.lineSeparator());
        sb.append(doc.body().text());
        return sb.toString();
    }

    public String getTitle() // preia titlul documentului
    {
        String title = doc.title();
        // System.out.println("Titlul site-ului: " + title);
        return title;
    }

    public String getKeywords() // preia cuvintele cheie
    {
        Element keywords = doc.selectFirst("meta[name=keywords]");
        String keywordsString = "";
        if (keywords == null) {
            // System.out.println("Nu exista tag-ul <meta name=\"keywords\">!");
        } else {
            keywordsString = keywords.attr("content");
            // System.out.println("Cuvintele cheie au fost preluate!");
        }
        return keywordsString;
    }

    public String getDescription() // preia descrierea site-ului
    {
        Element description = doc.selectFirst("meta[name=description]");
        String descriptionString = "";
        if (description == null) {
            // System.out.println("Nu exista tag-ul <meta name=\"description\">!");
        } else {
            descriptionString = description.attr("content");
            // System.out.println("Descrierea site-ului a fost preluata!");
        }
        return descriptionString;
    }

    public String getRobots() // preia lista de robots
    {
        Element robots = doc.selectFirst("meta[name=robots]");
        String robotsString = "";
        if (robots == null) {
            // System.out.println("Nu exista tag-ul <meta name=\"robots\">!");
        } else {
            robotsString = robots.attr("content");
            // System.out.println("Lista de robots a site-ului a fost preluata!");
        }
        return robotsString;
    }

    public Set<String> getLinks() // preia link-urile de pe site (ancorele)
    {
        Elements links = doc.select("a[href], A[href]");
        Set<String> URLs = new HashSet<String>();
        for (Element link : links) {
            String absoluteLink = link.attr("abs:href"); // facem link-urile relative sa fie absolute

            /*
            if (absoluteLink.contains(baseUri)) // ignoram legaturile interne
            {
                continue;
            }
            */

            // cautam eventuale ancore in link-uri
            int anchorPosition = absoluteLink.indexOf('#');
            if (anchorPosition != -1) // daca exista o ancora (un #)
            {
                // stergem partea cu ancora din link
                StringBuilder tempLink = new StringBuilder(absoluteLink);
                tempLink.replace(anchorPosition, tempLink.length(), "");
                absoluteLink = tempLink.toString();
            }

            // vrem doar link-uri care contin documente HTML
            try {
                URL absoluteLinkURL = new URL(absoluteLink);
                String path = absoluteLinkURL.getPath();
                String extension = path.substring(path.lastIndexOf(".") + 1);
                if (!extension.isEmpty()) // daca link-ul are extensie de document
                {
                    // verificam sa fie document HTML
                    if (!(path.endsWith("html") || path.endsWith("htm")))
                    {
                        continue;
                    }
                }

                // nu vrem sa adaugam duplicate, asa incat folosim o colectie de tip Set
                URLs.add(absoluteLink);
            } catch (MalformedURLException e)
            {
                // System.out.println("EROARE: URL eronat: " + absoluteLink);
                continue;
            }
        }
        // System.out.println("Link-urile de pe site au fost preluate!");
        return URLs;
    }
}
