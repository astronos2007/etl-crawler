import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

public class HTTPClient {
    private String userAgent;
    private String resourceFolder;
    private int lastStatus;
    /*
    lastStatus = cod de eroare intern
    Semnificatii:
    1 - nu se poate gasi IP-ul pentru domeniu folosind DNS Solver-ul propriu
    2 - prea multe redirectionari
    3 - pagina mutata, protocol nou nesuportat
    4 - timp de asteptare depasit
    5 - socket-ul nu poate determina automat IP-ul pentru domeniu
    6 - problema I/O cu socket-ul
     */

    private DNSResolver dns;

    public HTTPClient(String userAgent, String resSaveFolder)
    {
        this.userAgent = userAgent;
        resourceFolder = resSaveFolder;

        // servere de nume
        String nsTuiasi = "81.180.223.1";
        String nsOrange = "62.217.193.1";
        String nsGoogle = "8.8.4.4";
        String nsDigitalOcean = "ns1.digitalocean.com";

        dns = new DNSResolver(nsTuiasi, 53);
    }

    // preia o resursa web folosind protocolul HTTP 1.1
    public String getResource(String resName, String domainName, int port, int numberOfRetries)
    {
        // construim cererea HTTP
        StringBuilder requestBuilder = new StringBuilder();

        // linia de cerere
        requestBuilder.append("GET " + resName + " HTTP/1.1\r\n");

        // antetul Host
        requestBuilder.append("Host: " + domainName + "\r\n");

        // antetul User-Agent
        requestBuilder.append("User-Agent: " + userAgent + "\r\n");

        // antetul Connection
        requestBuilder.append("Connection: close\r\n");

        // final de cerere
        requestBuilder.append("\r\n");

        // terminam de construit cererea, convertind in sir de caractere
        String httpRequest = requestBuilder.toString();
        // System.out.println("Cerere HTTP construita.");

        // aflam IP-ul (host-ul) folosind DNS solver-ul propriu
        String host = dns.solveDomain(domainName, null, null);
        if (host == null) // solver-ul DNS nu a reusit preluarea IP-ului
        {
            // host = domainName;
            System.out.println("[DNS_SOLVER] EROARE: Nu se poate determina IP-ul pentru domeniul \"" + domainName + "\".");
            lastStatus = 1;
            return null;
        }

        Socket tcpSocket = null;
        try {
            // deschidem socket-ul TCP
            tcpSocket = new Socket(host, port);
            tcpSocket.setSoTimeout(1000); // setam un timeout de 1 secunda

            DataOutputStream outToServer = new DataOutputStream(tcpSocket.getOutputStream()); // buffer de iesire (pt cerere)
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream())); // buffer de intrare (pt raspuns)

            // trimitem cererea
            outToServer.writeBytes(httpRequest);
            // System.out.println("Cerere trimisa catre serverul " + host + ".");

            // preluam raspunsul de la server
            // System.out.println("Raspuns de la server: \n");

            String responseLine;
            boolean responseOK = false;
            boolean responseMovedPermanently = false;
            boolean responseMovedTemporarily = false;
            boolean responseFound = false;

            // prima linie este linia de stare, ce contine codul de raspuns
            responseLine = inFromServer.readLine();
            if (responseLine.contains("200 OK"))
            {
                responseOK = true;
                lastStatus = 200;
            }
            else if (responseLine.contains("301")) // Moved Permanently
            {
                responseMovedPermanently = true;
                lastStatus = 301;
            }
            else if (responseLine.contains("302")) // Found
            {
                responseFound = true;
                lastStatus = 302;
            }
            else if (responseLine.contains("307")) // Moved Temporarily
            {
                responseMovedTemporarily = true;
                lastStatus = 307;
            }
            else if (responseLine.contains("404"))
            {
                lastStatus = 404;
                return null;
            }
            // System.out.println(responseLine);

            // afisam si restul liniilor din antet
            String location = ""; // ne intereseaza noua locatie in caz de raspuns 301
            while ((responseLine = inFromServer.readLine()) != null)
            {
                if (responseLine.equals("")) // sfarsit de antet -> deci urmeaza continutul raspunsului
                {
                    break;
                }
                if (responseLine.startsWith("Location:")) // avem un raspuns 301 Moved Permanently sau 302 Found
                {
                    location = responseLine.replace("Location: ", "");
                }
                // System.out.println(responseLine);
            }

            String htmlFilePath;
            if (responseOK) // salvam continutul paginii doar daca raspunsul este 200 OK
            {
                // construim continutul paginii trimise de server
                StringBuilder pageBuilder = new StringBuilder();
                while ((responseLine = inFromServer.readLine()) != null) {
                    pageBuilder.append(responseLine + System.lineSeparator());
                }

                // construim calea de salvare a resursei
                htmlFilePath = resourceFolder + "/" + domainName + resName;
                if (!(htmlFilePath.endsWith(".html") || htmlFilePath.endsWith("htm")) && !resName.equals("/robots.txt"))
                {
                    if (!htmlFilePath.endsWith("/"))
                    {
                        htmlFilePath += "/";
                    }
                    htmlFilePath += "index.html";
                }

                File file = new File(htmlFilePath);
                File parentDirectory = file.getParentFile();
                if (!parentDirectory.exists())
                {
                    parentDirectory.mkdirs();
                }

                // salvam resursa
                BufferedWriter writer = new BufferedWriter(new FileWriter(htmlFilePath));
                writer.write(pageBuilder.toString());
                writer.close();
                // System.out.println("\nContinutul raspunsului a fost plasat in fisierul \"" + htmlFilePath + "\".");

                if (numberOfRetries > 0)
                {
                    System.out.println("[CLIENT_HTTP] Pagina gasita la locatia noua \"http://" + domainName + resName + "\".");
                }
            }
            else if (responseMovedPermanently || responseFound || responseMovedTemporarily) // 301 Moved Permanently sau 302 Found sau 307 Moved Temporarily
            {
                // inchidem socket-ul
                tcpSocket.close();

                if (numberOfRetries > 3)
                {
                    System.out.println("[CLIENT_HTTP] EROARE: Prea multe redirectionari pe domeniul \"" + domainName + "\".");
                    lastStatus = 2;
                    return null;
                }

                // refacem cererea pentru noua locatie
                URL newLocation = new URL(location);
                if (!newLocation.getProtocol().equals("http"))
                {
                    System.out.println("[CLIENT_HTTP] EROARE: Pagina mutata permanent sau temporar. Protocol nou nesuportat! (" + location + ")");
                    lastStatus = 3;
                    return null;
                }
                String newPath = newLocation.getPath();
                String newDomain = newLocation.getHost();
                int newPort = newLocation.getPort();
                if (newPort == -1)
                {
                    newPort = 80;
                }
                System.out.println("[CLIENT_HTTP] Pagina mutata permanent sau temporar la locatia \"http://" + newDomain + newPath + "\". Se incearca refacerea cererii HTTP...");
                return getResource(newPath, newDomain, newPort, numberOfRetries + 1);
            }
            else {
                // inchidem socket-ul
                tcpSocket.close();
                return null;
            }

            // inchidem socket-ul
            tcpSocket.close();

            return htmlFilePath;
        }
        catch (SocketTimeoutException ste)
        {
            System.out.println("[CLIENT_HTTP] EROARE: Timp de asteptare depasit pentru descarcarea \"http://" + domainName + resName + "\".");
            lastStatus = 4;
            return null;
        }
        catch (UnknownHostException uhe)
        {
            System.out.println("[CLIENT_HTTP] EROARE: Socket-ul nu poate determina IP-ul pentru domeniul \"" + domainName + "\".");
            lastStatus = 5;
            return null;
        }
        catch (IOException ioe)
        {
            System.out.println("[CLIENT_HTTP] EROARE: Problema socket I/O.");
            lastStatus = 6;
            return null;
        }
        finally
        {
            if (tcpSocket != null)
            {
                try
                {
                    tcpSocket.close();
                } catch (IOException ioe)
                {
                    System.out.println("[CLIENT_HTTP] EROARE: Nu s-a putut inchide socket-ul.");
                }
            }
        }
    }

    public int getLastStatus()
    {
        return lastStatus;
    }
}
