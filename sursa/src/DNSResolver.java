import javafx.util.Pair;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DNSResolver {
    private String dnsServer;
    private int port;

    // ----------------------------- CACHE DNS -----------------------------
    // Cache-ul DNS este format din perechi de forma <domeniu, (IP, momentExpirare)>
    private HashMap<String, Pair<String, Long>> cache;
    // ---------------------------------------------------------------------

    public DNSResolver(String server, int port)
    {
        dnsServer = server;
        this.port = port;
        cache = new HashMap<>();
    }

    // rezolva o cerere DNS pentru domeniul specificat
    String solveDomain(String domain, String authority, String oldDomain) // oldDomain = vechiul nume (in caz de raspuns CNAME)
    {
        // ----------------------------- VERIFICARE CACHE DNS -----------------------------
        if (cache.containsKey(domain)) // daca cache-ul local contine domeniul
        {
            // verificam data expirarii (in secunde)
            long expiryDate = cache.get(domain).getValue();
            long now = System.nanoTime() / 1000000000;

            if (expiryDate > now) // intrarea in cache este actuala
            {
                String ip = cache.get(domain).getKey();
                // System.out.println("[DNS_CACHE] Se returneaza IP-ul din cache (" + ip + ") pentru domeniul \"" + cacheDomain + "\".");
                return ip; // returnam IP-ul din cache, fara sa mai facem cererea
            }
        }
        // --------------------------------------------------------------------------------

        // iau un array de octeti de dimensiune 12 + strlen() + 6
        byte[] requestByteArray = new byte[12 + domain.length() + 6];

        // setez octetii care trebuie pentru cerere

        // Identifier
        Random r = new Random();
        int identifier = r.nextInt(1 << 16 - 1); // intreg pe doi octeti

        // luam MSB si LSB din ultimii 2 octeti din int
        byte[] identifierBytes = ByteBuffer.allocate(4).putInt(identifier).array();
        byte MSB = identifierBytes[2];
        byte LSB = identifierBytes[3];

        requestByteArray[0] = MSB;
        requestByteArray[1] = LSB;

        // Recursion Desired
        requestByteArray[2] = 0x01;

        // Question Count -> 1 intrebare
        requestByteArray[5] = 0x1;

        // de la octetul 12 incepe Question Name
        int questionIndex = 12;

        byte questionBuffer[] = new byte[domain.length()];
        int k = 0; // indicele din buffer-ul temporar
        char[] domainNameChars = domain.toCharArray();
        for (int j = 0; j < domain.length(); ++j) // parsam domeniul si il stocam in formatul specific
        {
            if (domainNameChars[j] != '.')
            {
                questionBuffer[k++] = (byte)domainNameChars[j];
            }
            else // am ajuns la punct
            {
                // setam numarul de caractere
                requestByteArray[questionIndex++] = (byte)k;

                // punem toate caracterele de dinaintea punctului curent
                for (int i = 0; i < k; ++i)
                {
                    requestByteArray[questionIndex++] = questionBuffer[i];
                }

                // resetam buffer-ul pentru urmatorul punct
                k = 0;
            }

            // daca am ajuns la final, punem si ultimele caractere
            if (j == domain.length() - 1)
            {
                // setam numarul de caractere
                requestByteArray[questionIndex++] = (byte)k;

                // punem toate caracterele de dinaintea punctului curent
                for (int i = 0; i < k; ++i)
                {
                    requestByteArray[questionIndex++] = questionBuffer[i];
                }
            }
        }
        // setam terminatorul de nume
        requestByteArray[questionIndex] = 0x0;

        // Question Type -> 1 = adresa IP
        requestByteArray[questionIndex + 2] = 0x1;

        // Question Class -> 1 = clasa internet
        requestByteArray[questionIndex + 4] = 0x1;

        String nsGiven = null; // daca s-a obtinut nume de domeniu (autoritate)
        String cNameGiven = null; // daca s-a obtinut nume canonic
        try
        {
            // construim un datagram cu destinatarul IP-ul server-ului DNS, portul 53
            DatagramSocket datagramSocket = new DatagramSocket();
            datagramSocket.setSoTimeout(1000); // setam un timeout de 1 secunda
            InetAddress IP = InetAddress.getByName((authority == null) ? dnsServer : authority);
            DatagramPacket requestPacket = new DatagramPacket(requestByteArray, requestByteArray.length, IP, port);

            // System.out.println("S-a construit pachetul de cerere DNS pentru numele de domeniu \"" + domain + "\".");
            /*
            for (int i = 0; i < requestByteArray.length; ++i)
            {
                System.out.print('\t');
                System.out.printf("[0x%02X] ", requestByteArray[i]);
                if ((i + 1) % 9 == 0)
                {
                    System.out.println();
                }
            }
            System.out.println();
            */

            // trimitem pachetul la server-ul DNS
            datagramSocket.send(requestPacket);
            // System.out.println("Pachetul a fost trimis catre server-ul " + (authority == null) ? dnsServer : authority + ".");

            // preluam raspunsul de la server
            // DatagramSocket responseDatagramSocket = new DatagramSocket(port);
            byte[] responseByteBuffer = new byte[512];

            // il punem intr-un buffer de 512 octeti
            DatagramPacket responsePacket = new DatagramPacket(responseByteBuffer, 512);
            // System.out.println("Se asteapta raspuns de la server...");

            try
            {
                datagramSocket.receive(responsePacket);
                // System.out.println("Pachetul de raspuns a fost primit cu succes:");
            } catch (SocketTimeoutException ste)
            {
                System.out.println("[DNS_TIMEOUT] Timp de raspuns prea mare pentru rezolvarea domeniului \"" + domain + "\".");
                datagramSocket.close();
                return null;
            }

            /*
            for (int i = 0; i < responseByteBuffer.length; ++i)
            {
                System.out.print('\t');
                System.out.printf("[0x%02X] ", responseByteBuffer[i]);
                if ((i + 1) % 10 == 0)
                {
                    System.out.println();
                }
            }
            System.out.println();
            */
            datagramSocket.close();

            // parsam raspunsul primit
            // verificam intai identificatorul sa se potriveasca cu cel al cererii
            LSB = responseByteBuffer[1];
            MSB = responseByteBuffer[0];
            int receivedIdentifier = (((0xFF) & MSB) << 8) | (0xFF & LSB);

            if (identifier != receivedIdentifier)
            {
                return null;
            }
            else
            {
                // System.out.println("Identificatorii se potrivesc: " + receivedIdentifier);
            }

            // verificam validitatea raspunsului -> sa nu se fi produs vreo eroare
            /* adica octetul al 4-lea, RCode:
            [1 -> RA = 1 pentru ca e raspuns
            000 -> obligatoriu, pentru departajare
            0000] -> Response Code 0 inseamna ca nu avem erori pentru cerere si raspunsul e ceea ce am dorit
             */
            int errorCode = responseByteBuffer[3] & 0x0F; // ultimii 4 biti sunt codul de raspuns
            if (errorCode != 0x00 && errorCode != 0x03) // daca e 3 (NXDOMAIN), tot putem avea informatii utile in raspuns
            {
                // System.out.println("[DNS_SOLVER] EROARE: RCode = " + errorCode);
                return null;
            }
            else
            {
                // System.out.println("Nicio eroare produsa: RCode 0 -> OK");
            }
            // System.out.println();

            // verificam numarul de raspunsuri primite (Answer Record Count)
            LSB = responseByteBuffer[7];
            MSB = responseByteBuffer[6];
            int numberOfResponses = (((0xFF) & MSB) << 8) | (0xFF & LSB);
            // System.out.println("Numarul de raspunsuri primite: " + numberOfResponses);

            // verificam si alte informatii primite
            // Name Server Count -> informatii despre autoritati
            LSB = responseByteBuffer[9];
            MSB = responseByteBuffer[8];
            int numberOfAuthorityInfos = (((0xFF) & MSB) << 8) | (0xFF & LSB);
            // System.out.println("Numarul de informatii despre autoritati primite: " + numberOfAuthorityInfos);

            // Additional Record Count -> informatii aditionale primite
            LSB = responseByteBuffer[11];
            MSB = responseByteBuffer[10];
            int numberOfAdditionalRecords = (((0xFF) & MSB) << 8) | (0xFF & LSB);
            // System.out.println("Numarul de informatii aditionale primite: " + numberOfAdditionalRecords);

            // indicele octetului de unde incep Resource Records
            int answerIndex = 12 + domain.length() + 6; // pentru ca server-ul mentine informatiile din cererea clientului
            // System.out.println("Raspunsul incepe de la octetul " + answerIndex);

            // System.out.println();

            if (numberOfAdditionalRecords + numberOfAuthorityInfos + numberOfResponses == 0) // daca nu am primit nimic
            {
                // System.out.println("Nicio informatie primita de la server.");
                return null;
            }

            // System.out.println("Informatiile primite de la server sunt:");
            int recordIndex = 1;
            while (recordIndex <= numberOfResponses + numberOfAdditionalRecords + numberOfAuthorityInfos)
            {
                // Resource Name
                // preluam numele de particula recursiv
                String particleName = getParticlePointer(answerIndex, responseByteBuffer);
                // System.out.println(responseByteBuffer[answerIndex] & 0xFF);

                // trebuie sa sarim peste un numar de octeti dependent de tipul de particula
                if ((responseByteBuffer[answerIndex] & 0xFF) < 192) // dimensiune de particula
                {
                    // System.out.println("Dimensiune de particula!");
                    answerIndex += particleName.length() + 1;
                }
                else // pointer
                {
                    // System.out.print("Pointer - ");

                    // calculam indicele de octet
                    // int responseByteIndex = ((responseByteBuffer[answerIndex] & 0x3F) << 8) | (responseByteBuffer[answerIndex + 1] & 0xFF);
                    // System.out.println("Index: " + responseByteIndex);

                    answerIndex += 2; // pointer-ul e pe 2 octeti
                }
                // System.out.println(particleName);
                // particleName = particleName.substring(0, particleName.length() - 1);
                // System.out.print("(" + recordIndex + ") Numele de particula: " + particleName);

                // Record Type (2 octeti)
                MSB = responseByteBuffer[answerIndex++];
                LSB = responseByteBuffer[answerIndex++];
                int recordType = (((0xFF) & MSB) << 8) | (0xFF & LSB);
                // System.out.print(" | Record Type: " + recordType);

                // Record Class (2 octeti)
                MSB = responseByteBuffer[answerIndex++];
                LSB = responseByteBuffer[answerIndex++];
                int recordClass = (((0xFF) & MSB) << 8) | (0xFF & LSB);
                // System.out.print(" | Record Class: " + recordClass);

                // TTL (Time To Live) - 4 octeti
                byte b3 = responseByteBuffer[answerIndex++];
                byte b2 = responseByteBuffer[answerIndex++];
                byte b1 = responseByteBuffer[answerIndex++];
                byte b0 = responseByteBuffer[answerIndex++];
                long TTL = ((0xFF & b3) << 24) | ((0xFF & b2) << 16) | ((0xFF & b1) << 8) | (0xFF & b0);

                /*
                long hours = TimeUnit.MILLISECONDS.toHours(TTL);
                TTL -= TimeUnit.HOURS.toMillis(hours);
                long minutes = TimeUnit.MILLISECONDS.toMinutes(TTL);
                TTL -= TimeUnit.MINUTES.toMillis(minutes);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(TTL);
                */
                // System.out.print(" | TTL: " + hours + "h " + minutes + "m " + seconds + "s");

                // Record Data Length (2 octeti)
                MSB = responseByteBuffer[answerIndex++];
                LSB = responseByteBuffer[answerIndex++];
                int dataLength = (((0xFF) & MSB) << 8) | (0xFF & LSB);

                // daca Data Length = 4 si Record Type = 1, raspunsul contine adresa IPv4
                if ((domain + ".").endsWith(particleName) && dataLength == 4 && recordType == 1 && recordClass == 1)
                {
                    // construim adresa IPv4
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < dataLength; ++i)
                    {
                        sb.append(responseByteBuffer[answerIndex++] & 0xFF);
                        sb.append('.');
                    }
                    sb.deleteCharAt(sb.length() - 1); // stergem ultimul punct, pus in plus
                    String ip = sb.toString();
                    // System.out.println(" | Adresa IPv4: " + ip);

                    // ----------------------------- ACTUALIZARE CACHE DNS -----------------------------
                    long now = System.nanoTime() / 1000000000;
                    cache.put(domain, new Pair<>(ip, TTL + now));
                    if (oldDomain != null)
                    {
                        cache.put(oldDomain, new Pair<>(ip, TTL + now));
                    }
                    // ---------------------------------------------------------------------------------
                    return ip;
                }
                if (recordType == 2 && recordClass == 1) // raspunsul contine un server de nume
                {
                    // preluam numele de particula recursiv
                    nsGiven = getParticlePointer(answerIndex, responseByteBuffer);

                    // sarim peste dataLength octeti
                    answerIndex += dataLength;

                    nsGiven = nsGiven.substring(0, nsGiven.length() - 1);
                    // System.out.println(" | Server de nume: " + nsGiven);
                }
                else if (recordType == 5 && recordClass == 1) // raspunsul contine un nume canonic
                {
                    // preluam numele de particula recursiv
                    cNameGiven = getParticlePointer(answerIndex, responseByteBuffer);

                    // sarim peste dataLength octeti
                    answerIndex += dataLength;

                    cNameGiven = cNameGiven.substring(0, cNameGiven.length() - 1);
                    // System.out.println(" | Nume canonic: " + cNameGiven);
                }
                ++recordIndex;
            }

            // s-a gasit un nume canonic, incercam sa refacem cererea pentru numele canonic obtinut
            if (cNameGiven != null)
            {
                System.out.println("[DNS_SOLVER] Se reface cererea DNS pentru numele canonic \"" + cNameGiven + "\" catre server-ul \"" + dnsServer + "\".");
                return solveDomain(cNameGiven, dnsServer, domain);
            }
            // s-a gasit un nume de autoritate, incercam sa adresam cererea catre un NS furnizat de server
            if (nsGiven != null)
            {
                System.out.println("[DNS_SOLVER] Se reface cererea DNS pentru domeniul \"" + domain + "\" catre autoritatea \"" + nsGiven + "\".");
                return solveDomain(domain, nsGiven, null);
            }
        }
        catch (UnknownHostException uhe)
        {
            System.out.println("[DNS_SOLVER] EROARE: Server DNS eronat: " + dnsServer);
            return null;
        }
        catch (SocketTimeoutException ste)
        {
            System.out.println("[DNS_SOLVER] EROARE: Timp de asteptare prea mare pentru rezolvarea domeniului \"" + domain + "\".");
            return null;
        }
        catch (SocketException se)
        {
            System.out.println("[DNS_SOLVER] EROARE: Problema socket.");
            return null;
        }
        catch (IOException ioe)
        {
            System.out.println("[DNS_SOLVER] EROARE: Problema socket I/O.");
            return null;
        }
        catch (ArrayIndexOutOfBoundsException aioobe) // pachet DNS corupt
        {
            return null;
        }

        return null;
    }

    // functie care preia o particula de nume folosind un pointer sau o dimensiune de particula trimisa ca index
    // functia este recursiva, pentru ca putem avea pointeri la alti pointeri
    private String getParticlePointer(int pointerIndex, byte[] buffer)
    {
        if ((buffer[pointerIndex] & 0xFF) == 0x0) // cat timp nu am ajuns la octetul terminator de nume
        {
            return "";
        }

        if ((buffer[pointerIndex] & 0xFF) >= 192) // iar am gasit pointer
        {
            // calculam indicele de octet
            int newPointerIndex = ((buffer[pointerIndex] & 0x3F) << 8) | (buffer[pointerIndex + 1] & 0xFF);
            return getParticlePointer(newPointerIndex, buffer);
        }

        // am ajuns pe dimensiune de particula, atunci construim sirul de caractere
        int currentNumberOfCharacters = buffer[pointerIndex++] & 0xFF;
        StringBuilder currentElement = new StringBuilder();
        for (int i = 0; i < currentNumberOfCharacters; ++i) // preluam cate o parte de particula
        {
            currentElement.append((char)(buffer[pointerIndex + i] & 0xFF));
        }

        // trecem la elementul urmator (daca exista)
        pointerIndex += currentNumberOfCharacters;
        return (currentElement.toString() + "." + getParticlePointer(pointerIndex, buffer));
    }
}