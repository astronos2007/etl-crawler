import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Indexer {


    public static void main(String[] args) throws IOException {




        Scanner queryScanner = new Scanner(System.in);

        // meniul afisat utilizatorului
        do {
            System.out.println("1. Creare index direct + indice \"tf\"");
            System.out.println("2. Creare index indirect + indice \"idf\"");
            System.out.println("3. Incarcare index indirect in memorie (pentru cautarea booleana)");
            System.out.println("4. Cautare booleana");
            System.out.println("5. Creare vectori asociati documentelor HTML");
            System.out.println("6. Incarcare vectori asociati in memorie (pentru cautarea vectoriala)");
            System.out.println("7. Cautare vectoriala");
            System.out.println("8. Iesire");

            System.out.print("Optiunea dvs: ");
            Scanner reader = new Scanner(System.in);
            int option = reader.nextInt();
            System.out.println();

            switch (option)
            {
                case 1:

                    break;
                case 2:

                    break;
                case 3:

                    break;
                case 4:

                    break;
                case 5:
                case 6:

                    break;
                case 7:

                case 8:
                    System.exit(0);
                default:
                    System.out.println("\nEROARE: Optiunea nu exista!");
            }

            System.out.print("\nApasati o tasta pentru continuare...");
            Scanner cont = new Scanner(System.in);
            cont.nextLine();

            // stergere output din consola
            System.out.print("\033[H\033[2J\n");
            System.out.flush();
            Runtime.getRuntime().exec("clear");
        } while (true);
    }
}
