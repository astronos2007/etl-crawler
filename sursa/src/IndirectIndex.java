import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class IndirectIndex {
    private static TreeMap<String, HashMap<String, Integer>> indirectIndexCollection = null;
    private static boolean indirectIndexLoaded = false;

    public static TreeMap<String, HashMap<String, Integer>> indirectIndex(WebsiteInfoIndexer websiteInfo) throws IOException
    {
        TreeMap<String, HashMap<String, Integer>> indirectIndex = new TreeMap<>();
        Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();

        String websiteFolder = websiteInfo.getWebsiteFolder();

        TreeMap<String, Double> idf = new TreeMap<>(); // pentru stocarea idf-ului
        int numberOfDocuments = 0; // pentru calculul idf-ului

        // pentru obtinerea fisierului de mapare, numit "indirectindex.map", aflat in directorul radacina
        Writer mapFileWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(websiteFolder + "indirectindex.map"), "utf-8"));
        HashMap<String, String> mapFile = new HashMap<>();

        // pentru parcurgerea directoarelor, folosim o coada
        LinkedList<String> folderQueue = new LinkedList<>();

        // pornim cu folder-ul radacina
        folderQueue.add(websiteFolder);

        while (!folderQueue.isEmpty()) // cat timp nu mai sunt foldere copil de parcurs
        {
            // preluam un folder din coada
            String currentFolder = folderQueue.pop();
            File folder = new File(currentFolder);
            File[] listOfFiles = folder.listFiles();

            // ii parcurgem lista de fisiere / foldere
            try {
                for (File file : listOfFiles)
                {
                    // daca am ajuns pe un fisier, verificam sa fie fisier de tip index direct, creat anterior
                    if (file.isFile() && file.getAbsolutePath().endsWith(".directindex.json"))
                    {
                        String fileName = file.getAbsolutePath();
                        String docName = fileName.replace(".directindex.json", "");

                        // preluam fisierul JSON cu indexul direct si il parsam
                        Type directIndexType = new TypeToken<HashMap<String, Integer>>(){}.getType();
                        HashMap<String, Integer> directIndex = gsonBuilder.fromJson(new String(Files.readAllBytes(file.toPath())), directIndexType);
                        // System.out.println("[PARSARE] Am parsat fisierul JSON \"" + fileName + "\".");

                        // stocam indexul indirect al fisierului curent
                        TreeMap<String, HashMap<String, Integer>> localIndirectIndex = new TreeMap<>();
                        for(Map.Entry<String, Integer> entry : directIndex.entrySet()) // luam fiecare cuvant si stocam numarul de aparitii si documentul din care face parte
                        {
                            String word = entry.getKey();
                            int numberOfApparitions = entry.getValue();

                            // adaugam intrarea in TreeMap-ul local
                            if (localIndirectIndex.containsKey(word)) // daca acel cuvant exista in TreeMap
                            {
                                // il adaugam in vectorul de aparitii
                                HashMap<String, Integer> apparitions = localIndirectIndex.get(word);
                                apparitions.put(docName, numberOfApparitions);
                                // localIndirectIndex.put(word, numberOfApparitions);
                            }
                            else
                            {
                                HashMap<String, Integer> apparitions = new HashMap<>();
                                apparitions.put(docName, numberOfApparitions);
                                localIndirectIndex.put(word, apparitions);
                            }

                            // adaugam intrarea in TreeMap-ul final
                            if (indirectIndex.containsKey(word)) // daca acel cuvant exista in TreeMap
                            {
                                // il adaugam in vectorul de aparitii
                                HashMap<String, Integer> apparitions = indirectIndex.get(word);
                                apparitions.put(docName, numberOfApparitions);
                                // indirectIndex.put(word, numberOfApparitions);
                            }
                            else
                            {
                                HashMap<String, Integer> apparitions = new HashMap<>();
                                apparitions.put(docName, numberOfApparitions);
                                indirectIndex.put(word, apparitions);
                            }
                        }

                        // scriem fisierul JSON cu index-ul indirect
                        Writer writer = new BufferedWriter(new OutputStreamWriter(
                                new FileOutputStream(docName + ".indirectindex.json"), "utf-8"));
                        writer.write(gsonBuilder.toJson(localIndirectIndex));
                        writer.close();

                        // System.out.println("[INDEX_INDIRECT_LOCAL] Am creat index-ul indirect in fisierul JSON \"" + docName + ".indirectindex.json\".");

                        // adaugam documentul curent, impreuna cu index-ul indirect asociat in fisierul de mapare
                        mapFile.put(docName, docName + ".indirectindex.json");

                        // pentru calculul idf
                        ++numberOfDocuments;
                    }
                    else if (file.isDirectory()) // daca este folder, il punem in coada
                    {
                        folderQueue.add(file.getAbsolutePath());
                    }
                }
            } catch (NullPointerException e) {
                // System.out.println("Nu exista fisiere in folderul \"" + currentFolder + "\"!");
            }
        }

        // cream fisierul JSON concatenat de index indirect
        Writer indirectIndexWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(websiteFolder + "indirectindex.json"), "utf-8"));
        indirectIndexWriter.write(gsonBuilder.toJson(indirectIndex));
        indirectIndexWriter.close();

        // System.out.println(System.lineSeparator());
        // System.out.println("[INDEX_INDIRECT] Am creat fisierul de index indirect \"" + websiteFolder + "indirectindex.json\".");

        // scriem fisierul de mapare JSON
        mapFileWriter.write(gsonBuilder.toJson(mapFile));
        mapFileWriter.close();
        // System.out.println("[MAPARE] Am creat fisierul de mapare \"" + websiteFolder + "indirectindex.map\".");

        // pentru calcul idf
        // iteram peste fiecare cuvant din index-ul indirect
        for (String word : indirectIndex.keySet())
        {
            int numberOfDocumentsMatched; // numarul de documente in care apare cuvantul curent
            numberOfDocumentsMatched = indirectIndex.get(word).size();
            idf.put(word, Math.log((double)numberOfDocuments / (numberOfDocumentsMatched)));
        }

        Writer idfWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(websiteFolder + "idf.json"), "utf-8"));
        idfWriter.write(gsonBuilder.toJson(idf));
        idfWriter.close();

        return indirectIndex;
    }

    // functia care incarca index-ul indirect in memorie
    public static TreeMap<String, HashMap<String, Integer>> loadIndirectIndex(String indirectIndexFile, boolean global) throws IOException
    {
        if (indirectIndexLoaded && global) // daca incarcam index-ul global, nu are rost sa fie incarcat decat o singura data
        {
            return indirectIndexCollection;
        }

        TreeMap<String, HashMap<String, Integer>> indirectIndex = new TreeMap<>();
        JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(indirectIndexFile), "UTF-8"));

        // parsam JSON-ul manual, obiect cu obiect
        reader.beginObject();
        while(reader.hasNext())
        {
            // aici se citeste fiecare cuvant
            String word = reader.nextName();

            HashMap<String, Integer> currentWordApparitions = new HashMap<>();

            // aici incep detaliile despre cuvant (document -> numar de aparitii)
            reader.beginObject();
            while (reader.hasNext())
            {
                currentWordApparitions.put(reader.nextName(), reader.nextInt());
            }
            reader.endObject();

            indirectIndex.put(word, currentWordApparitions);
        }
        reader.endObject();

        indirectIndexLoaded = true;
        indirectIndexCollection = indirectIndex;
        return indirectIndex;
    }
}
